package Core;

/**
 * Clase singleton, únicamente lo tengo para no tener que ir yendo a la clase MainGameLoop
 * @author Lucas
 *
 */
public class SWindow extends Window {

	private static SWindow instance;
	
	private SWindow(Field f) {
		super(f);
		instance = this;
	}
	
	public static SWindow getWindow(Field f) {
		if(instance == null) {
			new SWindow(f);
		}else {
			instance.changeField(f);
		}

		return instance;
		
	}
	
	public static SWindow getWindow() {
		return instance;		
	}
	
	public static void clear() {
		instance.changeField(new Field());
	}

}
