package PipoLeyenda;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import Core.Sprite;
import PipoLeyenda.SpriteDeriveds.Background;
import PipoLeyenda.SpriteDeriveds.FallEntity;
import PipoLeyenda.SpriteDeriveds.Platform;
import PipoLeyenda.SpriteDeriveds.GuiSprite;
import PipoLeyenda.SpriteDeriveds.Item;
import PipoLeyenda.SpriteDeriveds.Pipo;
import PipoLeyenda.SpriteDeriveds.Enemies.Adult;
import PipoLeyenda.SpriteDeriveds.Enemies.Drone;
import PipoLeyenda.SpriteDeriveds.Enemies.Enemy;

/**
 * This class is who has the patterns of each level, Background, items,
 * platforms, foreground and enemies
 * 
 * @author Lucas
 *
 */
public class MapGenerator{

	private static Random r = new Random();
	public static int floorHeight = (MainGameLoop.wHeight / 40) * 37;

	/**
	 * @param level - Level to be played
	 * @return - The array of sprites of the background of level
	 */
	public static ArrayList<Sprite> loadBG(int level){
		ArrayList<Sprite> bg = new ArrayList<>();

		switch (level){
		case 1:
			Background sky = new Background(MainGameLoop.wWidth * 2, MainGameLoop.wHeight, "res/BGIG/sky.png");
			bg.add(sky);

			// {{ Build 1
			int[][] build1Pos = getBackgroundsPositions(level).get("build1");
			int buildPosCenterX = build1Pos[0][0] + ((build1Pos[0][1] - build1Pos[0][0]) / 2);
			Background build1 = new Background(build1Pos[0][0], build1Pos[1][0], build1Pos[0][1], build1Pos[1][1],
					"res/BGIG/build1.png");
			bg.add(build1);
			Background build1Door = new Background(buildPosCenterX - 50, build1Pos[1][1] - 160, buildPosCenterX + 50,
					build1Pos[1][1], "res/BGIG/door1.png");
			bg.add(build1Door);

			Background build1Window1 = new Background(build1Pos[0][0] + ((buildPosCenterX - build1Pos[0][0]) / 2) - 45,
					build1Pos[1][1] - 160, build1Pos[0][0] + ((buildPosCenterX - build1Pos[0][0]) / 2) + 45,
					build1Pos[1][1] - 70, "res/BGIG/window1.png");
			bg.add(build1Window1);
			Background build1Window2 = new Background(buildPosCenterX + ((buildPosCenterX - build1Pos[0][0]) / 2) - 45,
					build1Pos[1][1] - 160, buildPosCenterX + ((buildPosCenterX - build1Pos[0][0]) / 2) + 45,
					build1Pos[1][1] - 70, "res/BGIG/window1.png");
			bg.add(build1Window2);

			// TODO This three backgrounds have a platform, so they will be moved into
			// platforms function and be platforms instead
			Background build1ToldoG = new Background(buildPosCenterX - 170, build1Pos[1][1] - 214,
					buildPosCenterX + 170, build1Pos[1][1] + 30, "res/BGIG/toldoG.png");
			bg.add(build1ToldoG);
			Background build1WindowUpper1 = new Background(
					build1Pos[0][0] + ((buildPosCenterX - build1Pos[0][0]) / 2) - 60, build1Pos[1][1] - 465,
					build1Pos[0][0] + ((buildPosCenterX - build1Pos[0][0]) / 2) + 60, build1Pos[1][1] - 315,
					"res/BGIG/window2.png");
			bg.add(build1WindowUpper1);
			Background build1WindowUpper2 = new Background(
					buildPosCenterX + ((buildPosCenterX - build1Pos[0][0]) / 2) - 60, build1Pos[1][1] - 465,
					buildPosCenterX + ((buildPosCenterX - build1Pos[0][0]) / 2) + 60, build1Pos[1][1] - 315,
					"res/BGIG/window2.png");
			bg.add(build1WindowUpper2);
			// }}

//			String[] bgpaths = {"res/BGIG/sidewalk.png", "res/BGIG/sidewalkBrick.png", "res/BGIG/sidewalkCross.png"};
//
//			Background sidewalk;
//			
//			for (int i = 1; i <= ancho; i++) {
//				int rand = r.nextInt(bgpaths.length);
//				
//				if(rand == 2) {
//					sidewalk = new Background(100*(i-1), (MainGameLoop.wHeight/40)*32, 100*i+200, MainGameLoop.wHeight, bgpaths[rand]);
//					i+=2;
//				}else
//					sidewalk = new Background(100*(i-1), (MainGameLoop.wHeight/40)*32, 100*i, MainGameLoop.wHeight, bgpaths[rand]);
//
//				bg.add(sidewalk);
//				
//			}

			int ancho = InGame.background.x2 % 100 != 0 ? (InGame.background.x2 / 100) + 1
					: (InGame.background.x2 / 100);
			for (int i = 1; i <= ancho; i++){
				Background sidewalk = new Background(100 * (i - 1), MainGameLoop.field.getHeight() - 75, 100 * i,
						MainGameLoop.field.getHeight(), "res/BGIG/sidewalkBrick2.png");
				bg.add(sidewalk);
//				Generador de farolos
//				if(r.nextInt(15) == 1) {
//					Background farolo = new Background(80*(i-1), MainGameLoop.wHeight-90-355, 80*i, MainGameLoop.wHeight-90, "res/BGIG/faroloR.png");
//					bg.add(farolo);
//				}
			}

		default:
			break;
		}

		return bg;
	}

	/**
	 * @param level - Level to be played
	 * @return - The array of platforms of the level
	 */
	public static ArrayList<Platform> loadPlatforms(int level){
		ArrayList<Platform> platforms = new ArrayList<>();

		switch (level){
		case 1:
//			platforms.add(new Floor(0,(MainGameLoop.wHeight/32)*24,(MainGameLoop.wWidth/32)*5));
			Platform build1ToldoG = new Platform((MainGameLoop.wWidth / 40) * 5 + (MainGameLoop.wWidth / 40) / 2,
					(MainGameLoop.wHeight / 40) * 25 + (MainGameLoop.wHeight / 40) / 2,
					(MainGameLoop.wWidth / 40) * 10 + (MainGameLoop.wWidth / 40) / 2);
			platforms.add(build1ToldoG);
			Platform basura = new Platform((MainGameLoop.wWidth / 40) * 1 + (MainGameLoop.wWidth / 40) / 2,
					(MainGameLoop.wHeight / 40) * 32, (MainGameLoop.wWidth / 40) * 2 + (MainGameLoop.wWidth / 40) / 4,
					(MainGameLoop.wHeight / 40) * 4 + 11, "res/miVida2.png");
			basura.collisionBox(0, 0, (MainGameLoop.wHeight / 40) * 1 + (MainGameLoop.wHeight / 40) * 1 / 2, 0);
			platforms.add(basura);

			// {{ Build1

			int[][] build1Pos = getBackgroundsPositions(level).get("build1");
			int buildPosCenterX = build1Pos[0][0] + ((build1Pos[0][1] - build1Pos[0][0]) / 2);

			Platform build1WindowUpper1 = new Platform(build1Pos[0][0] + ((buildPosCenterX - build1Pos[0][0]) / 2) - 60,
					build1Pos[1][1] - 320, 120);
			platforms.add(build1WindowUpper1);
			Platform build1WindowUpper2 = new Platform(buildPosCenterX + ((buildPosCenterX - build1Pos[0][0]) / 2) - 60,
					build1Pos[1][1] - 320, 120);
			platforms.add(build1WindowUpper2);

			// }}

			break;
		default:
			break;
		}

		return platforms;
	}

	/**
	 * @param level - Level to be played
	 * @return - The array of sprites of the foreground of level
	 */
	public static ArrayList<Sprite> loadFG(int level){
		ArrayList<Sprite> fg = new ArrayList<>();

		switch (level){
		case 1:

			fg.add(new Item("pantsus", (MainGameLoop.wWidth / 40) * 10, (MainGameLoop.wHeight / 40) * 20,
					"res/GUI/pantsus.gif"));

			int[][] build1Pos = getBackgroundsPositions(level).get("build1");
			int buildPosCenterX = build1Pos[0][0] + ((build1Pos[0][1] - build1Pos[0][0]) / 2);
			fg.add(new Item("pantsus", build1Pos[0][0] + ((buildPosCenterX - build1Pos[0][0]) / 2) - 25,
					build1Pos[1][1] - 470, "res/GUI/pantsus.gif"));

			fg.add(new FallEntity("Maceta1", buildPosCenterX + ((buildPosCenterX - build1Pos[0][0]) / 2),
					build1Pos[1][1] - 465, 50, 50, "res/black.png", 150));

			fg.add(new Item("bleach", (MainGameLoop.wWidth / 40) * 34, (MainGameLoop.wHeight / 40) * 33,
					"res/GUI/bleach.gif"));

//			Generador de farolas aleatorias
//			int ancho = InGame.background.x2%100 != 0 ? (InGame.background.x2/100)+1 : (InGame.background.x2/100);
//			for (int i = 4; i <= ancho-3; i++) {
//				if(r.nextInt(15)==1) {
//					Background farolo = new Background(100*(i-1), MainGameLoop.wHeight-355, 147, MainGameLoop.wHeight, "res/BGIG/faroloR.png", 0);
//					fg.add(farolo);
//				}
//			}
			break;
		case 2:

			fg.add(new Item("bleach", (MainGameLoop.wWidth / 40) * 30, (MainGameLoop.wHeight / 40) * 33,
					"res/GUI/bleach.gif"));
			fg.add(new Item("pantsus", (MainGameLoop.wWidth / 40) * 32, (MainGameLoop.wHeight / 40) * 33,
					"res/GUI/pantsus.gif"));
			fg.add(new Item("bleach", (MainGameLoop.wWidth / 40) * 34, (MainGameLoop.wHeight / 40) * 33,
					"res/GUI/bleach.gif"));

			break;
		default:
			break;

		}
		return fg;
	}

	/**
	 * @param level - Level to be played
	 * @return - The list of the enemies of the level
	 */
	public static ArrayList<Enemy> loadEnemies(int level){
		ArrayList<Enemy> enemies = new ArrayList<>();
		int[] basicKnockback = { 3, -5 };
		int[] higherKnockback = { 2, -7 };
		switch (level){
		case 1:
			enemies.add(new Adult((MainGameLoop.wWidth / 40) * 30, (MainGameLoop.wHeight / 40) * 25, 550, 10, 3,
					basicKnockback));
			enemies.add(new Drone((MainGameLoop.wWidth / 40) * 30, (MainGameLoop.wHeight / 40) * 19, 300, 200, 2,
					higherKnockback));
			break;

		default:
			break;
		}
		return enemies;
	}

	/**
	 * @param level - Level to be played
	 * @return - Returns the positions of some structures that are important in the
	 *         level for making easier to work with platforms
	 */
	public static HashMap<String, int[][]> getBackgroundsPositions(int level){
		HashMap<String, int[][]> bgSizes = new HashMap<String, int[][]>();
		switch (level){
		case 1:
			int[][] build1Pos = { { 15, 677 }, { -240, 620 } };
			bgSizes.put("build1", build1Pos);
			break;
		}
		return bgSizes;
	}

	/**
	 * @param level - The level to be played
	 * @return The spawnpoint of {@link Pipo} in every level
	 */
	public static int[] getSpawnpoint(int level){
		int[] sp = { 0, 0 };
		switch (level){
		case 1:
			sp[0] = 218;
			sp[1] = 624;
			break;

		default:
			break;
		}
		return sp;
	}

	/**
	 * @param level - Level to be played
	 * @return - The quantity of bleach that is needed on each level
	 */
	public static int bleachNeaded(int level){
		switch (level){
		case 1:
			return 1;
		case 2:
			return 2;
		case 3:
			return 3;
		default:
			break;
		}
		return 0;
	}

	/**
	 * Generates a grid of 40x40 in front of the screen
	 * 
	 * @param enable  - If the grid should be enabled or not
	 * @param sprites - The list of the sprites where the grid should be, normaly
	 *                the foreground
	 * @return - The same list of <b>sprites</b> but with the grid
	 */
	public static ArrayList<Sprite> grids(boolean enable, ArrayList<Sprite> sprites){
		if(enable){
			int lines = 40;
			for (int i = 1; i < lines; i++){
				GuiSprite aux = new GuiSprite("grid", 0, (MainGameLoop.wHeight / lines) * i, MainGameLoop.wWidth, 1,
						"res/red.png");
				System.out.println(aux.x1 + " " + aux.y1 + " : " + aux.x2 + " " + aux.y2);
				sprites.add(aux);

			}
			for (int i = 1; i < lines; i++){
				GuiSprite aux = new GuiSprite("grid", (MainGameLoop.wWidth / lines) * i, 0, 1, MainGameLoop.wHeight,
						"res/red.png");
				System.out.println(aux.x1 + " " + aux.y1 + " : " + aux.x2 + " " + aux.y2);
				sprites.add(aux);

			}
		} else{
			for (Sprite s : sprites){
				if(s.name.equals("grid"))
					sprites.remove(s);
			}
		}

		return sprites;
	}

}
