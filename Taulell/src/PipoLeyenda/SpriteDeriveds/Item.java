package PipoLeyenda.SpriteDeriveds;

import Core.Sprite;

/**
 * Creates an item Actualy there are only pantsus and bleach, if the list gets
 * bigger they will be in an enum
 * 
 * @author Lucas
 */
public class Item extends Sprite{

	/**
	 * Create an item of custom size
	 * 
	 * @param name - Inner engine name
	 * @param x1   - Top left position horizontally
	 * @param y1   - Top left position vertically
	 * @param x2   - Bottom right position horizontally
	 * @param y2   - Bottom right position vertically
	 * @param path - The source of the sprite
	 */
	public Item(String name, int x1, int y1, int x2, int y2, String path){
		super(name, x1, y1, x2, y2, path);
		this.terrain = false;
		this.physicBody = false;
	}

	/**
	 * Create an item of fixed size
	 * 
	 * @param name - Inner engine name
	 * @param x1   - Top left position horizontally
	 * @param y1   - Top left position vertically
	 * @param path - The source of the sprite
	 */
	public Item(String name, int x1, int y1, String path){
		super(name, x1, y1, x1 + 50, y1 + 74, path);
		this.terrain = false;
		this.physicBody = false;
	}
}
