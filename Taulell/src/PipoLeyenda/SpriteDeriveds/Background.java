package PipoLeyenda.SpriteDeriveds;

import Core.Sprite;

/**
 * For Sprites that will be behind of the scene
 * 
 * @author Lucas
 */
public class Background extends Sprite{

	/**
	 * Backgrounds of all screen size
	 * 
	 * @param width  - The width of the sprite
	 * @param height - The height of the sprite
	 */
	public Background(int width, int height){
		super("Background", 0, 0, width, height, "res/city.png");
		this.solid = false;
		this.physicBody = false;
		this.terrain = false;
	}

	/**
	 * Component of Background
	 * 
	 * @param width  - The width of the sprite
	 * @param height - The height of the sprite
	 * @param path   - The source of the sprite
	 */
	public Background(int width, int height, String path){
		super("Background", 0, 0, width, height, path);
		this.solid = false;
		this.physicBody = false;
		this.terrain = false;
	}

	/**
	 * Squared Gui sprites
	 * 
	 * @param name - Inner engine name
	 * @param x1   - Top left position horizontally
	 * @param y1   - Top left position vertically
	 * @param size - Both (horizontal and vertical size)
	 * @param path - The source of the sprite
	 */
	public Background(int x1, int y1, int size, String path){
		super("Background", x1, y1, x1 + size, y1 + size, path);
		this.solid = false;
		this.physicBody = false;
		this.terrain = false;
	}

	/**
	 * No squared Gui sprites
	 * 
	 * @param name   - Inner engine name
	 * @param x1     - Top left position horizontally
	 * @param y1     - Top left position vertically
	 * @param width  - The width of the sprite
	 * @param height - The height of the sprite
	 * @param path   - The source of the sprite
	 */
	public Background(int x1, int y1, int x2, int y2, String path){
		super("Background", x1, y1, x2, y2, path);
		this.solid = false;
		this.physicBody = false;
		this.terrain = false;
	}

	/**
	 * No squared Gui sprites
	 * 
	 * @param name   - Inner engine name
	 * @param x1     - Top left position horizontally
	 * @param y1     - Top left position vertically
	 * @param width  - The width of the sprite
	 * @param height - The height of the sprite
	 * @param path   - The source of the sprite
	 */
	public Background(int x1, int y1, int width, int y2, String path, int inutil){
		super("Background", x1, y1, x1 + width, y2, path);
		this.solid = false;
		this.physicBody = false;
		this.terrain = false;
	}
}
