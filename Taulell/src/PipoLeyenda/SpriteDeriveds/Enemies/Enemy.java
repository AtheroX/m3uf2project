package PipoLeyenda.SpriteDeriveds.Enemies;

import Core.Sprite;

/**
 * Creates an enemy They have gravity and moves X distance constantly
 * 
 * @author Lucas
 */
public abstract class Enemy extends Sprite{

	int minX;
	int maxX;
	int moveSpeed;

	public int[] knockBack = { 2, -5 };
	public boolean movingLeft = true, stuned = false;

	public int timeToStun = 0, timeStuned = 0;

	/**
	 * Creates an enemy
	 * 
	 * @param name      - Inner engine name
	 * @param x1        - Top left position horizontally
	 * @param y1        - Top left position vertically
	 * @param width     - The width of the enemy
	 * @param height    - The height of the enemy
	 * @param minX      - The leftmost distance that this enemy can move
	 * @param maxX      - The rightmost distance that this enemy can move
	 * @param speed     - The speed of the enemy
	 * @param knockback - The knockback of the enemy
	 * @param path      - The source of the sprite
	 */
	public Enemy(int x1, int y1, int width, int height, int minX, int maxX, int speed, int[] knockback, String path){
		super("Enemy", x1, y1 - 20, x1 + width, y1 + height, path);
		collisionBox(0, 0, 0, -20);
		this.minX = (this.x1 + (this.x2 - this.x1) / 2) - minX; // Changes from object relative position to world
																// position
		this.maxX = (this.x1 + (this.x2 - this.x1) / 2) + maxX;
		this.moveSpeed = speed;
		this.trigger = true;
		this.solid = true;
		this.knockBack = knockback;
	}

	/**
	 * Makes the enemy move, each children of Enemy makes it move diferently
	 */
	public abstract void move();

	/**
	 * For scrolling Changes the minX and maxX to move acordly to scroll
	 * 
	 * @param scrollSpeed
	 */
	public void movePath(float scrollSpeed){
		this.minX -= scrollSpeed;
		this.maxX -= scrollSpeed;

	}

}
