package PipoLeyenda.SpriteDeriveds.Enemies;

import PipoLeyenda.MainGameLoop;

/**
 * Enemy that extends directly from {@link Enemy},
 * 
 * @author Lucas
 *
 */
public class Drone extends Enemy{

	/**
	 * Constructor for Drones with a preset sprite
	 * 
	 * @param x1        - Top left position horizontally
	 * @param y1        - Top left position vertically
	 * @param minX      - From this position, the leftmost position that this will
	 *                  move
	 * @param maxX      - From this position, the rightmost position that this will
	 *                  move
	 * @param speed     - The speed of the movement
	 * @param knockback - Knockback values, first is horizontally, second is
	 *                  vertically
	 */
	public Drone(int x1, int y1, int minX, int maxX, int speed, int[] knockback){
		super(x1, y1, (MainGameLoop.wWidth / 40) * 3, (MainGameLoop.wHeight / 40) * 2, minX, maxX, speed, knockback,
				"res/Enemies/Drone1.png");
	}

	/**
	 * Constructor for Adults with a preset sprite
	 * 
	 * @param x1        - Top left position horizontally
	 * @param y1        - Top left position vertically
	 * @param minX      - From this position, the leftmost position that this will
	 *                  move
	 * @param maxX      - From this position, the rightmost position that this will
	 *                  move
	 * @param speed     - The speed of the movement
	 * @param knockback - Knockback values, first is horizontally, second is
	 *                  vertically
	 * @param path      - The source of the sprite
	 */
	public Drone(int x1, int y1, int minX, int maxX, int speed, int[] knockback, String path){
		super(x1, y1, (MainGameLoop.wWidth / 40) * 3, (MainGameLoop.wHeight / 40) * 2, minX, maxX, speed, knockback,
				path);
	}

	/**
	 * Moves horizontally from minX to maxX Also, if is Stuned, it acts as a timer
	 */
	@Override
	public void move(){
		if(!stuned){
			if(movingLeft){
				if(x2 <= minX)
					movingLeft = false;
				else{
					x1 -= this.moveSpeed;
					x2 -= moveSpeed;
				}
			} else{
				if(x1 >= maxX)
					movingLeft = true;
				else{
					x1 += moveSpeed;
					x2 += moveSpeed;
				}
			}
		} else{
			timeStuned++;
			if(timeStuned >= timeToStun){
				stuned = false;
				timeToStun = 0;
				timeStuned = 0;
			}
		}
	}
}
