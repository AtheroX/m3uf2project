package PipoLeyenda.SpriteDeriveds;

import java.io.Serializable;

import Core.Sprite;
import PipoLeyenda.ButtonStates;

public class ShopItem extends Sprite implements Serializable{

	public int price;
	public boolean bought = false;
	public boolean buyable = true;
	public boolean selected = false;
	
	public ShopItem(String name, int x1, int y1, int x2, int y2, String path){
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
	}
	public ShopItem(String name, int x1, int y1, int width, int height, String path, int price){
		super(name, x1, y1, x1 + width, y1 + height, path);
		this.price = price;
		// TODO Auto-generated constructor stub
	}

	public ShopItem(String name, int x1, int y1, int width, int height, String path, int price, boolean buyable){
		super(name, x1, y1, x1 + width, y1 + height, path);
		this.price = price;
		this.buyable = buyable;
		// TODO Auto-generated constructor stub
	}

	public ButtonStates able(){
		if(buyable){
			if(bought){
				return ButtonStates.DONE;
			} else{
				return ButtonStates.UNLOCK;
			}
		} else{
			return ButtonStates.COMINGSOON;
		}
	}

	@Override
	public String toString(){
		return "" + price + ";" + bought + ";" + buyable + ";" + selected + ";" + name + ";" + x1 + ";" + y1 + ";" + x2 + ";" + y2 + ";" + path;
	}

}
