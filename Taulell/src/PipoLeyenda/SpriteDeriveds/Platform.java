package PipoLeyenda.SpriteDeriveds;

import Core.Sprite;

/**
 * For create a floor It can be platforms Platforms only have collision on their
 * top
 * 
 * @author Lucas
 */
public class Platform extends Sprite{

	/**
	 * A floor that is not a platform
	 * 
	 * @param name - Inner engine name
	 * @param x1   - Top left position horizontally
	 * @param y1   - Top left position vertically
	 * @param x2   - Bottom right position horizontally
	 * @param y2   - Bottom right position vertically
	 * @param path - The source of the image
	 */
	public Platform(String name, int x1, int y1, int x2, int y2, String path){
		super(name, x1, y1, x2, y2, path);
		this.terrain = true;
		this.unscrollable = true;
	}

	/**
	 * For Platforms that are invisible
	 * 
	 * @param x1    - Top left position horizontally
	 * @param y1    - Top left position vertically
	 * @param width - The width of the platform
	 */
	public Platform(int x1, int y1, int width){
		super("Platform", x1, y1, x1 + width, y1 + 20, "");
		this.terrain = true;
	}

	/**
	 * For Platforms that have a sprite
	 * 
	 * @param x1     - Top left position horizontally
	 * @param y1     - Top left position vertically
	 * @param width  - The width of the platform
	 * @param height - The height of the platform
	 * @param path   - The source of the image
	 */
	public Platform(int x1, int y1, int width, int height, String path){
		super("Platform", x1, y1, x1 + width, y1 + height, path);
		this.terrain = true;
	}
}
