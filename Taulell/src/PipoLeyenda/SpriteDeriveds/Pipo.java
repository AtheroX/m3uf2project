package PipoLeyenda.SpriteDeriveds;

import java.util.ArrayList;

import Core.Sprite;
import PipoLeyenda.InGame;
import PipoLeyenda.MainGameLoop;
import PipoLeyenda.MapGenerator;
import PipoLeyenda.SpriteDeriveds.Enemies.Adult;
import PipoLeyenda.SpriteDeriveds.Enemies.Enemy;

/**
 * Main character of the game
 * 
 * @author Lucas Ruiz Belmonte
 *
 */
public class Pipo extends Sprite{

	private static Pipo instance = null;

	public final static int WIDTH = 110;
	public final static int HEIGHT = 76;

	public final float MOVESPEED = 8f;
	public final float RUNSPEED = 2f;

	private final float JUMPFORCE = -8.5f;
	private final float GRAVITYFORCE = 0.22f;

	private boolean onGround = false;
	private boolean canJump = true;
	private int timeToBeHitted = 64;
	private int actualTimeToBeHitted = 0;
	private int minToScroll;
	private int maxToScroll;

	public boolean canBeHitted = true;
	public boolean lookingRight = true;
	public boolean canMove = true;
	public float moveX = 0f;
	public int bleachGauge = 3;

	// {{ SPRITES
	public static int actualSkin = 0;
	public static String[][] pipoImages = {
			{ "res/Pipo/dogIdleR.png", "res/Pipo/dogIdleL.png", "res/Pipo/dogRunR.gif", "res/Pipo/dogRunL.gif",
					"res/Pipo/dogIdleHitR.gif", "res/Pipo/dogIdleHitL.gif", "res/Pipo/dogRunHitR.gif",
					"res/Pipo/dogRunHitL.gif" },
			{ "res/red.png", "res/red.png", "res/red.png", "res/red.png", "res/red.png", "res/red.png", "res/red.png",
					"res/red.png" },
			{ "res/black.png", "res/black.png", "res/black.png", "res/black.png", "res/black.png", "res/black.png",
					"res/black.png", "res/black.png" } };
	// }}

	/**
	 * Creates the player in a world position Initializes the bleachGauge, the
	 * collision box, the scroll and the gravity
	 * 
	 * @param x1 - Horizontal position
	 * @param y1 - Vertical position
	 */
	private Pipo(int x1, int y1){
		super("Pipo", x1 - (WIDTH / 2), y1 - (HEIGHT / 2), x1 + (WIDTH / 2), y1 + (HEIGHT / 2),
				pipoImages[actualSkin][0]);
		bleachGauge = 3;
		collisionBox(10, 25, 0, 0);
		minToScroll = MainGameLoop.wWidth / 40 * 10;
		maxToScroll = MainGameLoop.wWidth / 40 * 31;

		setConstantForce(0, GRAVITYFORCE);

	}

	public static Pipo getInstance(int x1, int y1){
		if(instance == null){
			instance = new Pipo(x1, y1);
		} else if(instance.x1 != x1 || instance.y1 != y1){
			instance.x1 = x1 - (WIDTH / 2);
			instance.y1 = y1 - (HEIGHT / 2);
			instance.x2 = x1 + (WIDTH / 2);
			instance.y2 = y1 + (HEIGHT / 2);
			instance.canBeHitted = true;
			instance.lookingRight = true;
			instance.canMove = true;
			instance.bleachGauge = 3;
		}
		return instance;
	}

	public static Pipo getInstance(){
		return instance;
	}

	/**
	 * Moves the player and changes the image depending on its velocity, the
	 * direction that pipo's watching and if it can be hitted or not. Also acts as a
	 * scroll for every sprite and makes parallax like effect
	 */
	public void updateMove(){
		if(canMove){
			if(x2 > maxToScroll){ // DERECHA
				for (Sprite s : InGame.allSprites){
					if(s.name != "Pipo"){
						if(!s.unscrollable && InGame.background.x2 > MainGameLoop.wWidth && lookingRight){
							s.x1 -= moveX;
							s.x2 -= moveX;
							if(s instanceof Enemy){
								Enemy e = (Enemy) s;
								e.movePath(moveX);
							}
						}
					} else{
						if(!lookingRight || (InGame.background.x2 <= MainGameLoop.wWidth && x2 < InGame.background.x2)){
							x1 += moveX;
							x2 += moveX;
						}
					}
				}
			} else if(x1 < minToScroll){ // IZQUIERDA
				for (Sprite s : InGame.allSprites){
					if(s.name != "Pipo"){
						if(!s.unscrollable && InGame.background.x1 < 0 && !lookingRight){
							s.x1 -= moveX;
							s.x2 -= moveX;
							if(s instanceof Enemy){
								Enemy e = (Enemy) s;
								e.movePath(moveX);
							}
						}
					} else{
						if(lookingRight || (InGame.background.x1 >= 0 && x1 > InGame.background.x1)){
							x1 += moveX;
							x2 += moveX;
						}
					}
				}
			} else{
				x1 += moveX;
				x2 += moveX;
			}

			if(canBeHitted){
				if(lookingRight){
					if(moveX < 0.5f && moveX > -0.5f)
						this.changeImage(pipoImages[actualSkin][0]);
					else
						this.changeImage(pipoImages[actualSkin][2]);
				} else{
					if(moveX < 0.5f && moveX > -0.5f)
						this.changeImage(pipoImages[actualSkin][1]);
					else
						this.changeImage(pipoImages[actualSkin][3]);
				}
			} else{
				if(lookingRight){
					if(moveX < 0.5f && moveX > -0.5f)
						this.changeImage(pipoImages[actualSkin][4]);
					else
						this.changeImage(pipoImages[actualSkin][6]);
				} else{
					if(moveX < 0.5f && moveX > -0.5f)
						this.changeImage(pipoImages[actualSkin][5]);
					else
						this.changeImage(pipoImages[actualSkin][7]);
				}
			}
		}
	}

	/**
	 * If the player can jump and its on the ground then jump
	 */
	public void jump(){
		if(onGround && canJump)
			addVelocity(0, JUMPFORCE);
	}

	/**
	 * Checks if player is on floor, if it's not or if it just jumped
	 */
	public void checkGround(){
		if(!onGround){
			if(isGrounded(MainGameLoop.field)){
				onGround = true;
				canMove = true;
				setVelocity(0, 0);
			}
		} else{
			if(!isGrounded(MainGameLoop.field))
				onGround = false;
		}
	}

	/**
	 * Checks all collisions of the player for {@link Platform}, for {@link Enemy}
	 * and {@link Item} Platforms are separated of enemies and items because there
	 * is an ArrayList of platforms and there's no need to check their class
	 * procendence.
	 * 
	 * If you have been hitted it calls {@link #hit(Enemy)}
	 * 
	 * This is also the timer of beHitted and checks if the animation of be hitted
	 * should stop
	 * 
	 * Also moves {@link Dragable} objects
	 */
	public void checkCollisions(){
		checkForPlatformCollisions();

		// Checks for item and enemy collisions
		ArrayList<Sprite> colls = collidesWithField(MainGameLoop.field);
		for (Sprite s : colls){
			// TODO Change this an make it works by interface
			if(s instanceof Item){
				if(s.name.equals("bleach"))
					InGame.increaseBleach();
				else if(s.name.equals("pantsus"))
					InGame.increasePantsus();

				s.solid = false;
				s.delete();
			} else if(s instanceof Enemy){
				if(canBeHitted){
					Enemy e = (Enemy) s;
					hit(e);
					canMove = false;
					canBeHitted = false;
				}
			} else if(s instanceof Dragable){
				if((this.x2 - 30 < s.x1 && lookingRight) || (this.x1 + 30 > s.x2 && !lookingRight)){
					s.x1 += moveX;
					s.x2 += moveX;
				}
			}
		}

		if(!canBeHitted){
			if(actualTimeToBeHitted >= timeToBeHitted){
				canBeHitted = true;
				actualTimeToBeHitted = 0;
			}
			actualTimeToBeHitted++;
		}
	}

	/**
	 * Checks if the platforms are above or beyond of pipo Depending on this
	 * disables their collision
	 * 
	 * TODO this is the cause of {@link FallEntity} needs to be trigger, change it
	 * all
	 */
	private void checkForPlatformCollisions(){
		for (Sprite s : InGame.allPlatforms){
			if(s.y1 < this.y2)
				s.solid = false;
			else
				s.solid = true;
		}
	}

	/**
	 * When the game should pause this is called and pipo stops walking
	 */
	public void stop(){
		if(!this.path.equals(pipoImages[actualSkin][0]) && !this.path.equals(pipoImages[actualSkin][1])){
			if(lookingRight)
				this.changeImage(pipoImages[actualSkin][0]);
			else
				this.changeImage(pipoImages[actualSkin][1]);
		}
	}

	/**
	 * Called when the player have collisioned with an enemy Pipo's bleach desire
	 * lowers, updates that graphicaly, starts the timer of invulnerability and gets
	 * knockbacked
	 * 
	 * @param e - The enemy that have collisioned with pipo
	 */
	private void hit(Enemy e){
		if(!e.stuned){
			bleachGauge--;
			InGame.changeBleachGauge(bleachGauge);

			setVelocity(0, 0);

			if((this.x1 + (this.x2 - this.x1)) <= (e.x1 + (e.x2 - e.x1)))
				addVelocity(-e.knockBack[0], e.knockBack[1]);
			else
				addVelocity(e.knockBack[0], e.knockBack[1]);

			if(lookingRight)
				this.changeImage(pipoImages[actualSkin][4]);
			else
				this.changeImage(pipoImages[actualSkin][5]);
		}
	}

	/**
	 * Increases the bleach desire and updates graphically.
	 */
	public void increaseBleachGauge(){
		if(bleachGauge < 3){
			bleachGauge++;
			InGame.changeBleachGauge(bleachGauge);
		}
	}
}
