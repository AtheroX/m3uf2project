package PipoLeyenda.SpriteDeriveds;

import Core.Sprite;
import PipoLeyenda.InGame;
import PipoLeyenda.MainGameLoop;
import PipoLeyenda.MapGenerator;
import PipoLeyenda.SpriteDeriveds.Enemies.Enemy;

/**
 * Objects that can fall and if falls on a {@link Enemy} it will be
 * {@link Enemy#stuned}
 * 
 * @author Lucas
 *
 */
public class FallEntity extends Sprite implements Stunable, Dragable{

	final float GRAVITYFORCE = 0.12f;

	public boolean canMove = false, firstInstance = true;

	boolean onGround = false;
	Sprite stepOn = null;

	private int stunTime;

	/**
	 * 
	 * @param name     - Inner engine name
	 * @param x1       - Top left position horizontally
	 * @param y1       - Top left position vertically
	 * @param width    - The width of the sprite
	 * @param height   - The height of the sprite
	 * @param path     - The source of the sprite
	 * @param stunTime - Time that this object will stun a {@link Enemy}
	 */
	public FallEntity(String name, int x1, int y1, int width, int height, String path, int stunTime){
		super(name, x1, y1, x1 + width, y1 + height, path);
		this.stunTime = stunTime;
		this.trigger = true;

	}

	/**
	 * Makes this have gravity if it needs and stuns {@link Enemy}
	 */
	public void fall(){
		if(canMove){
			if(!this.onGround){
				setConstantForce(0, GRAVITYFORCE);
				stepOn = firstCollidesWithList(MapGenerator.loadPlatforms(InGame.actualLevel));
				if(stepOn != null){
					onGround = true;
				} else if(collidesWith(InGame.sueloGeneral)){
					stepOn = InGame.sueloGeneral;
					onGround = true;
				} else{
//					System.out.println(this.velocity[1]);
					Enemy e = (Enemy) firstCollidesWithList(InGame.allEnemies);
					if(e != null && this.velocity[1] >= 1.5d){
						wayForStunning(e);
					}
				}
			} else{
				if(stepOn != null){
					if(!collidesWith(stepOn)){
						stepOn = null;
						onGround = false;
					} else{
						this.setVelocity(0, 0);
						this.y1 = stepOn.y1 - (this.y2 - this.y1) - 1;
						this.y2 = stepOn.y1 - 1;

					}
				}
			}
		} else{
			physicBody = false;
		}
	}

	/**
	 * This is the way that this object stuns {@link Enemy}
	 * 
	 * @param enemy - The enemy to stun
	 */
	@Override
	public void wayForStunning(Enemy enemy){
		System.out.println(stunTime);
		enemy.stuned = true;
		enemy.timeToStun = stunTime;
		this.delete();
	}

}
