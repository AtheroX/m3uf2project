**La Leyenda de Pipo**

Eres pipo, un perro que tiene una filia extraña a la lejía, tu objetivo, morir por ella.
Tienes que obtener X de lejía para pasarte el nivel y hay 10 braguitas en cada nivel cómo coleccionables.
No todo es tan fácil, habrá gente que según ellos, se “preocupan por ti”, así que 
te quitarán la lejía y tus ganas de beber disminuirán, si disminuyen demasiado abortarás la misión.


Controles:
- Wasd: Movimiento
- Shift: Correr
 
[Ideas](https://docs.google.com/document/d/1aQiXGBi3RYl6hInRLEhXsV-sxPrw79c-8_gjR6hSx14/edit?usp=sharing)